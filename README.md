A very easy program. Once either list A or list B has no other items, append all items from the
 remaining list to list C in order. List C is now sorted list.
 
 Lyn Gabriel - Software Developer at http://www.lungsal.com/Bamboo-Stylus.html
 
 
 
 int num[20];

int main(int argc, const char * argv[]) {
    
    int n, i;
    
    printf("how many numbers?\n");
    scanf("%d",&n);
    printf("Pick numbers");
    
    for (i = 0; i < n; i++)
        scanf("%d", &num[i]);
    
    mergeSort(num, 0, n-1);
    
    for (i = 0; i < n; i++)
        printf("%d", num[i]);
    
    
    return 0;
}